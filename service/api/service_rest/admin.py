from django.contrib import admin
from .models import AutomobileVO, Appointment

@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = [
        "status",
        "date",
        "time",
        "reason",
        "vin",
    ]

@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = [
        "vin",
        "sold",
    ]
