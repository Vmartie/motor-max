from django.db import models

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=20, unique=True)


class Appointment(models.Model):
    STATUS_CHOICES = (
        ("created", "Created"),
        ("cancelled", "Cancelled"),
        ("finished", "Finished"),
    )
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default="created")
    date = models.DateField()
    time = models.TimeField()
    reason = models.TextField()
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
    )

    def finish(self):
        self.status = "finished"
        self.save()

    def cancel(self):
        self.status = "cancelled"
        self.save()
