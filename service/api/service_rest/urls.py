from django.urls import path
from .views import (api_technicians,
                    api_technician,
                    api_list_appointment,
                    api_appointment,
                    api_cancel_appointment,
                    api_finish_appointment,
                    api_inventory,
                )

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:pk>/", api_technician, name="api_technician"),
    path("appointments/", api_list_appointment, name="api_list_appointment"),
    path("appointments/", api_inventory, name="api_inventory"),
    path("appointments/<str:vin>/", api_appointment, name="api_appointment"),
    path("appointments/<str:vin>/cancel/", api_cancel_appointment, name="api_cancel_appointment"),
    path("appointments/<str:vin>/finish/", api_finish_appointment, name="api_finish_appointment"),
]
