import React, { useEffect, useState } from "react";

function SalesForm() {
  const [salespersons, setSalespersons] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [autos, setAutos] = useState([]);

  const [formData, setFormData] = useState({
    automobile: "",
    salesperson: "",
    customer: "",
    price: "",
  });

  const fetchData = async () => {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespersons(data.salespersons);
    }

    const customer = "http://localhost:8090/api/customers/";
    const custom = await fetch(customer);
    if (custom.ok) {
      const data = await custom.json();
      setCustomers(data.customers);
    }

    const automobile = "http://localhost:8100/api/automobiles/";
    const auto = await fetch(automobile);
    if (auto.ok) {
      const data = await auto.json();
      setAutos(data.autos);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8090/api/sales/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        automobile: "",
        salesperson: "",
        customer: "",
        price: "",
      });
    }
  };

  const handleFormChange = (event) => {
    const value = event.target.value;
    const inputName = event.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new Sale</h1>
          <form onSubmit={handleSubmit} id="create-sales-form">
            <div className="mb-3">
              <label htmlFor="automobile">Automobile VIN</label>
              <select
                value={formData.autos}
                onChange={handleFormChange}
                required
                name="automobile"
                id="automobile"
                className="form-select"
              >
                <option value="">Choose an automobile VIN...</option>
                {autos.map((auto) => {
                  return <option key={auto.vin}>{auto.vin}</option>;
                })}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="salesperson">Salesperson</label>
              <select
                value={formData.salesperson}
                onChange={handleFormChange}
                required
                name="salesperson"
                id="salesperson"
                className="form-select"
              >
                <option value="">Choose a Salesperson...</option>
                {salespersons.map((salesperson) => {
                  return (
                    <option key={salesperson.employee_id}>
                      {salesperson.employee_id}
                    </option>
                  );
                })}
              </select>
            </div>

            <div className="mb-3">
              <label htmlFor="customer">Customer</label>
              <select
                value={formData.customer}
                onChange={handleFormChange}
                required
                name="customer"
                id="customer"
                className="form-select"
              >
                <option value="">Choose a Customer...</option>
                {customers.map((customer) => {
                  return (
                    <option key={customer.first_name}>
                      {customer.first_name} {customer.last_name}
                    </option>
                  );
                })}
              </select>
            </div>

            <label htmlFor="price">Price</label>
            <div className="mb-3">
              <input
                onChange={handleFormChange}
                placeholder=""
                required
                type="number"
                name="price"
                id="price"
                className="form-control"
                value={formData.price}
              />
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesForm;
