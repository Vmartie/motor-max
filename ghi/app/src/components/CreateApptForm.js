import React, { useState, useEffect } from 'react';

function CreateApptForm() {
    const [tech, setTechnicians] = useState([])
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date: '',
        time: '',
        technician: '',
        reason: '',
    })

    const getData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.tech);
        }
    }
    useEffect(() => {
        getData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                vin: '',
                customer: '',
                date: '',
                time: '',
                technician: '',
                reason: '',
            });
        }
    }
    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setFormData({
            ...formData,
            [inputName]: value
        });
}
return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a service appointment</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">Vin</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.customer} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" />
                        <label htmlFor="customer">Customer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.date} placeholder="date" required type="date" name="date" id="date" className="form-control" />
                        <label htmlFor="date">Date</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} value={formData.time} placeholder="time" required type="time" name="time" id="time" className="form-control" />
                        <label htmlFor="time">Time</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} value={formData.technician} required id="technician" name="technician" className="form-select">
                            <option value="">Choose a technician</option>
                            {tech.map(tech => {
                                return (
                                    <option key={tech.id} value={tech.id}>
                                        {tech.first_name} {tech.last_name}
                                    </option>
                                );
                            })}
                        </select>
                        <div className="mb-3">
                            <label htmlFor="reason">Reason</label>
                            <textarea onChange={handleFormChange} value={formData.reason} required type="text" rows="3" name="reason" id="reason" className="form-control"></textarea>
                        </div>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
);
}
export default CreateApptForm;
