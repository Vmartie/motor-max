import { useEffect, useState } from "react";

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);
  const getData = async () => {
    const request = await fetch("http://localhost:8100/api/manufacturers/");
    if (request.ok) {
      const response = await request.json();
      setManufacturers(response.manufacturers);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <h1>Manufacturer</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers
            .sort((a, b) => a.id - b.id)
            .map((manufacturer) => {
              return (
                <tr key={manufacturer.name}>
                  <td>{manufacturer.name}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturerList;
