import { useEffect, useState } from "react";

function SalesPersonHistoryList() {
  const [sales, setSales] = useState([]);
  const [filterValue, setFilterValue] = useState("");
  const [filterType, setFilterType] = useState("");

  const getData = async () => {
    const request = await fetch("http://localhost:8090/api/sales/");
    if (request.ok) {
      const response = await request.json();
      setSales(
        response.sales.map((sale) => ({
          salespersonfirst: sale.salesperson.first_name,
          salespersonlast: sale.salesperson.last_name,
          customerfirst: sale.customer.first_name,
          customerlast: sale.customer.last_name,
          vin: sale.automobile,
          price: sale.price,
        }))
      );
    }
  };
  useEffect(() => {
    getData();
  }, []);

  function handleFilterTypeChange(e) {
    setFilterType(e.target.value);
  }

  function getFilterValue() {
    return sales
      .filter((sale) => sale[filterType].toLowerCase().includes(filterValue))
      .sort((a, b) => a.salespersonfirst - b.salespersonfirst);
  }

  return (
    <div>
      <h1>Salesperson History</h1>
      <select onChange={handleFilterTypeChange}>
        <option value="salespersonfirst salespersonlast">
          Salesperson First Name
        </option>
        <option value="salespersonlast">Salesperson Last Name</option>
      </select>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {getFilterValue().map((sale) => {
            return (
              <tr key={sale.id}>
                <td>
                  {sale.salespersonfirst} {sale.salespersonlast}
                </td>
                <td>
                  {sale.customerfirst} {sale.customerlast}
                </td>
                <td>{sale.vin}</td>
                <td>{sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default SalesPersonHistoryList;
