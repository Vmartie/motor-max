import { useEffect, useState } from "react";

function VehicleModelList() {
  const [models, setModels] = useState([]);
  const getData = async () => {
    const request = await fetch("http://localhost:8100/api/models/");
    if (request.ok) {
      const response = await request.json();
      setModels(response.models);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <h1>Models</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img src={model.picture_url} /> {" "}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default VehicleModelList;
