import { useEffect, useState } from "react";

function CustomerList() {
  const [customers, setCustomers] = useState([]);

  const getData = async () => {
    const request = await fetch("http://localhost:8090/api/customers/");
    if (request.ok) {
      const response = await request.json();
      setCustomers(response.customers);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <h1>Customer</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers
            .sort((a, b) => a.id - b.id)
            .map((customer) => {
              return (
                <tr key={customer.first_name}>
                  <td>{customer.first_name}</td>
                  <td>{customer.last_name}</td>
                  <td>{customer.phone_number}</td>
                  <td>{customer.address}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}

export default CustomerList;
