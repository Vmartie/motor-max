import React, { useEffect, useState } from "react";

function ApptList() {
  const [appointments, setAppointment] = useState([]);
  const [autos, setInventory] = useState([]);
  const getData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointment(data.appointments);
    }
  };
  const getInventory = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");
    if (response.ok) {
      const data = await response.json();
      setInventory(data.autos);
    }
  };
  useEffect(() => {
    getData();
    getInventory();
  }, []);

  const handlecancel = async (vin) => {
    try {
      const response = await fetch(
        `http://localhost:8080/api/appointments/${vin}/cancel/`,
        {
          method: "PUT",
        }
      );
      if (response.ok) {
        const updatedAppointments = appointments.filter(
          (appointment) => appointment.vin !== vin
        );
        setAppointment(updatedAppointments);
      } else {
        console.error("Failed to cancel appointment");
      }
    } catch (error) {
      console.error("Error during deletion.", error);
    }
  };
  const handlefinish = async (vin) => {
    try {
      const response = await fetch(
        `http://localhost:8080/api/appointments/${vin}/finish/`,
        {
          method: "PUT",
        }
      );
      if (response.ok) {
        const updatedAppointments = appointments.filter(
          (appointment) => appointment.vin !== vin
        );
        setAppointment(updatedAppointments);
      } else {
        console.error("Failed to finish appointment");
      }
    } catch (error) {
      console.error("Error during deletion.", error);
    }
  };

  return (
    <div>
      <h1>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vin</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointments) => {
            return (
              <tr key={appointments.id}>
                <td>{appointments.vin}</td>
                <td>
                  {autos.find((auto) => auto.vin === appointments.vin)
                    ? "Yes"
                    : "No"}
                </td>
                <td>{appointments.customer}</td>
                <td>{appointments.date}</td>
                <td>{appointments.time}</td>
                <td>
                  {appointments.technician.first_name}{" "}
                  {appointments.technician.last_name}
                </td>
                <td>{appointments.reason}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => handlecancel(appointments.vin)}
                  >
                    Cancel
                  </button>
                  <button
                    className="btn btn-success"
                    onClick={() => handlefinish(appointments.vin)}
                  >
                    Finish
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
export default ApptList;
