import MainPage from './MainPage';
import Nav from './Nav';
import TechList from './components/TechList';
import TechForm from './components/TechForm';
import ApptList from './components/ApptList';
import CreateApptForm from './components/CreateApptForm';
import ServiceHistoryList from './components/ServiceHistoryList';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import SalesPeopleForm from "./components/SalesPeopleForm";
import SalesPeopleList from "./components/SalesPeopleList";
import CustomerForm from "./components/CustomerForm";
import CustomerList from "./components/CustomerList";
import SalesForm from "./components/SalesForm";
import SalesList from "./components/SalesList";
import ManufacturerForm from "./components/ManufacturerForm";
import ManufacturerList from "./components/ManufacturerList";
import VehicleModelForm from "./components/VehicleModelForm";
import VehicleModelList from "./components/VehicleModelList";
import AutomobilesForm from "./components/AutomobilesForm";
import AutomobilesList from "./components/AutomobilesList";
import SalesPersonHistoryList from "./components/SalesPersonHistoryList";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/" element={<TechList />} />
          <Route path="/technicians/new" element={<TechForm />} />
          <Route path="/appointments/" element={<ApptList />} />
          <Route path="/appointments/new" element={<CreateApptForm />} />
          <Route path="/appointments/history" element={<ServiceHistoryList />} />
          <Route path="/salespeople/" element={<SalesPeopleList />} />
          <Route path="/salespeople/new" element={<SalesPeopleForm />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/customers/" element={<CustomerList />} />

          <Route path="/sales/new" element={<SalesForm />} />
          <Route path="/sales/" element={<SalesList />} />

          <Route path="/manufacturer/new" element={<ManufacturerForm />} />
          <Route path="/manufacturer/" element={<ManufacturerList />} />

          <Route path="/models/new" element={<VehicleModelForm />} />
          <Route path="/models/" element={<VehicleModelList />} />

          <Route path="/automobiles/new" element={<AutomobilesForm />} />
          <Route path="/automobiles/" element={<AutomobilesList />} />
          <Route path="/history" element={<SalesPersonHistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
