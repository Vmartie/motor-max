import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="row">
            <div className="col-lg">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <NavLink className="navbar-brand" to="/">
                  CarCar
                </NavLink>
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/manufacturer/"
                  >
                    Manufacturer
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/manufacturer/new"
                  >
                    Create a Manufacturer
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/models/"
                  >
                    Models
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/models/new"
                  >
                    Create a Model
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/automobiles/"
                  >
                    Automobiles
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/automobiles/new"
                  >
                    Create an Automobile
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/salespeople/"
                  >
                    Salespeople
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/salespeople/new"
                  >
                    Add a Salesperson
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/customers/"
                  >
                    Customers
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="row">
              <div className="col-lg">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                    <NavLink
                      className="nav-link active"
                      aria-current="page"
                      to="/customers/new"
                    >
                      Add a Customer
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      className="nav-link active"
                      aria-current="page"
                      to="/sales/"
                    >
                      Sales
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      className="nav-link active"
                      aria-current="page"
                      to="/sales/new"
                    >
                      Add a Sale
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      className="nav-link active"
                      aria-current="page"
                      to="/history"
                    >
                      Salesperson History
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link active" to="/technicians/">Technicians</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link active" to="/technicians/new">Add a Technician</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link active" to="/appointments/">Service Appointments</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link active" to="/appointments/new">Create a Service Appointment</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link active" to="/appointments/history">Service History</NavLink>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
